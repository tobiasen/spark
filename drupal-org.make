; This is a standard make file for packaging the distribution along with any
; contributed modules/themes or external libraries. Some examples are below.
; See http://drupal.org/node/159730 for more details.

api = 2
core = 7.x

; Contributed modules; standard.

projects[responsive_bartik][type] = theme
projects[responsive_bartik][version] = 1.0
projects[responsive_bartik][subdir] = contrib

projects[zurb_foundation][type] = theme
projects[zurb_foundation][version] = 5.0-alpha8
projects[zurb_foundation][subdir] = contrib
projects[zurb_foundation][patch][2332927] = https://www.drupal.org/files/issues/zurb-foundation-quickedit-2332927-6.patch

projects[ctools][type] = module
projects[ctools][version] = 1.4
projects[ctools][subdir] = contrib

projects[entity][type] = module
projects[entity][version] = 1.5
projects[entity][subdir] = contrib

projects[jquery_update][type] = module
projects[jquery_update][version] = 2.4
projects[jquery_update][subdir] = contrib

projects[json2][type] = module
projects[json2][version] = 1.1
projects[json2][subdir] = contrib

projects[libraries][type] = module
projects[libraries][version] = 2.2
projects[libraries][subdir] = contrib

projects[panels][type] = module
projects[panels][version] = 3.4
projects[panels][subdir] = contrib

projects[picture][type] = module
projects[picture][version] = 1.2
projects[picture][subdir] = contrib

projects[views][type] = module
projects[views][version] = 3.8
projects[views][subdir] = contrib

projects[l10n_update][type] = module
projects[l10n_update][version] = 1.0
projects[l10n_update][subdir] = contrib

projects[l10n_client][type] = module 
projects[l10n_client][version] = 1.3
projects[l10n_client][subdir] = contrib

projects[transliteration][type] = module
projects[transliteration][version] = 3.2
projects[transliteration][subdir] = contrib

; Contributed projects; Sparkish.
projects[ckeditor][type] = module
projects[ckeditor][version] = 1.16
projects[ckeditor][subdir] = contrib

projects[breakpoints][type] = module
projects[breakpoints][version] = 1.3
projects[breakpoints][subdir] = contrib

projects[navbar][type] = module
projects[navbar][version] = 1.4
projects[navbar][subdir] = contrib

projects[quickedit][type] = module
projects[quickedit][version] = 1.1
projects[quickedit][subdir] = contrib

projects[quickedit_tab][type] = module
projects[quickedit_tab][version] = 1.1
projects[quickedit_tab][subdir] = contrib

projects[ember][type] = theme
projects[ember][version] = 2.0-alpha2
;projects[ember][download][type] = git
;projects[ember][download][branch] = 7.x-1.x
projects[ember][subdir] = contrib

projects[gridbuilder][type] = module
projects[gridbuilder][version] = 1.0-alpha2
;projects[gridbuilder][download][type] = git
;projects[gridbuilder][download][branch] = 7.x-1.x
projects[gridbuilder][subdir] = contrib

projects[layout][type] = module
projects[layout][version] = 1.0-alpha6
;projects[layout][download][type] = git
;projects[layout][download][branch] = 7.x-1.x
projects[layout][subdir] = contrib

projects[every_field][type] = module
projects[every_field][version] = 1.x-dev
projects[every_field][subdir] = contrib

projects[responsive_preview][type] = module
projects[responsive_preview][version] = 1.1
; @todo Replace with 1.0 when that's available.
;projects[responsive_preview][download][type] = git
;projects[responsive_preview][download][branch] = 7.x-1.x
projects[responsive_preview][subdir] = contrib

; Contributed modules; SEO
projects[seo_checklist][type] = module
projects[seo_checklist][version] = 4.1
projects[seo_checklist][subdir] = contrib

projects[pathauto][type] = module 
projects[pathauto][version] = 1.2
projects[pathauto][subdir] = contrib

projects[globalredirect][type] = module
projects[globalredirect][version] = 1.5
projects[globalredirect][subdir] = contrib

projects[redirect][type] = module
projects[redirect][version] = 1.0-rc1 
projects[redirect][subdir] = contrib

projects[ga_tokenizer][type] = module
projects[ga_tokenizer][version] = 1.5
projects[ga_tokenizer][subdir] = contrib

projects[contact_google_analytics][type] = module
projects[contact_google_analytics][version] = 1.4
projects[contact_google_analytics][subdir] = contrib

projects[seo_checker][type] = module
projects[seo_checker][version] = 1.7 
projects[seo_checker][subdir] = contrib

projects[linkchecker][type] = module
projects[linkchecker][version] = 1.2
projects[linkchecker][subdir] = contrib

projects[subpathauto][type] = module
projects[subpathauto][version] = 1.3
projects[subpathauto][subdir] = contrib

projects[site_verify][type] = module
projects[site_verify][version] = 1.1
projects[site_verify][subdir] = contrib

projects[xmlsitemap][type] = module
projects[xmlsitemap][version] = 2.0
projects[xmlsitemap][subdir] = contrib

projects[site_map][type] = module
projects[site_map][version] = 1.2
projects[site_map][subdir] = contrib

projects[contentanalysis][type] = module
projects[contentanalysis][version] = 1.0-beta6
projects[contentanalysis][subdir] = contrib

projects[contentoptimizer][type] = module
projects[contentoptimizer][version] = 2.0-beta4
projects[contentoptimizer][subdir] = contrib

projects[google_analytics][type] = module
projects[google_analytics][version] = 2.0
projects[google_analytics][subdir] = contrib

projects[google_analytics_reports][type] = module
projects[google_analytics_reports][version] = 1.3
projects[google_analytics_reports][subdir] = contrib

projects[metatag][type] = module
projects[metatag][version] = 1.4
projects[metatag][subdir] = contrib

; Contributed modules; Security
projects[security_review][type] = module
projects[security_review][version] = 1.2
projects[security_review][subdir] = contrib

; Contributed modules; Helper
projects[token][type] = module
projects[token][version] = 1.5
projects[token][subdir] = contrib

projects[imagecache_token][type] = module
projects[imagecache_token][version] = 1.x-dev
projects[imagecache_token][subdir] = contrib

projects[diff][type] = module
projects[diff][version] = 3.2
projects[diff][subdir] = contrib

; Contributed modules; UX++
projects[module_filter][type] = module
projects[module_filter][version] = 1.8
projects[module_filter][subdir] = contrib

projects[simplified_menu_admin][type] = module
projects[simplified_menu_admin][version] = 1.0-beta2
projects[simplified_menu_admin][subdir] = contrib

; Libraries.
; NOTE: These need to be listed in http://drupal.org/packaging-whitelist.
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor%20for%20Drupal/edit/ckeditor_4.3.2_edit.zip

libraries[json2][download][type] = get
libraries[json2][download][url] = https://raw.github.com/douglascrockford/JSON-js/master/json2.js
libraries[json2][revision] = fc535e9cc8fa78bbf45a85835c830e7f799a5084

libraries[underscore][download][type] = get
libraries[underscore][download][url] = https://github.com/jashkenas/underscore/archive/1.5.2.zip

libraries[backbone][download][type] = get
libraries[backbone][download][url] = https://github.com/jashkenas/backbone/archive/1.1.0.zip

libraries[modernizr][download][type] = "get"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/v2.7.0.tar.gz"
